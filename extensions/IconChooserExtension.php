<?php

class IconChooserExtension extends DataExtension {

	private static $db = array(
		'Icon' => 'Varchar'
	);

	public function updateCMSFields(FieldList $fields) {
		$IconSources = (array)Config::inst()->get('IconChooser', 'sources');
		$allicons = array('No Icon', array('' => ''));
		foreach($IconSources as $Key => $IconSource) {
			// extract css
			if(!$IconSource['css']) {
				continue;
			}
			if(strpos($IconSource['css'], '//') !== 0) {
				$theme = Config::inst()->get('SSViewer', 'theme');
				$IconSource['css'] = THEMES_PATH.'/'.$theme.'/'.$IconSource['css'];
			} else {
				$IconSource['css'] = 'http:'.$IconSource['css'];
			}

			// get file
			$FileContents = file_get_contents($IconSource['css']);

			// initialise
			$icons = array();

			// run through file
			preg_match_all($IconSource['regex'], $FileContents, $match);
			foreach ($match[2] as $element) {
				if (strpos($element, ':before') === false) {
					continue;
				} else {
					$element = str_replace(':before', '', $element);
				}

				// icomoon self made icons with multiple paths
				if ($pos = strpos($element, ' ')) {
					$element = substr($element, 0, $pos);
				}

				// fa only
				if ($pos = strpos($element, ',')) {
					$element = substr($element, 0, $pos);
				}

				$element = $Key.'-'.trim($element);
				$icons[$element] = $element;
			}

			// add to list
			$allicons[$IconSource['name']] = $icons;
		}

		$fields->addFieldToTab('Root.Main', GroupedDropdownField::create('Icon', 'Select an icon', $allicons)
			->setEmptyString('None')
			->setAttribute('data-placeholder', 'Select an icon'));
	}

	public function IconMarkup($ExtraClass = '') {
		if($this->owner->Icon && $Parts = explode('-', $this->owner->Icon)) {
			$IconSources = (array)Config::inst()->get('IconChooser', 'sources');
			if(isset($IconSources[$Parts[0]]) && $IconSource = $IconSources[$Parts[0]]) {
				return str_replace(array('[iconclass]', '[extraclass]'), array($this->owner->Icon, $ExtraClass), $IconSource['markup']);
			}
		}
	}

}
